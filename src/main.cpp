// SDA   -   PIN10
// SCK   -   PIN13
// MOSI  -   PIN11
// MISO  -   PIN12
// IRQ   -   NC
// GND   -   GND
// RST   -   PIN9
// 3.3V  -   3.3V

#include <Arduino.h>  // arduino library
#include <SPI.h>      // Library for SPI communication
#include <MFRC522.h>  // RDID library

#define SS_PIN 10
#define RST_PIN 9

String prev_content;

MFRC522 rfid(SS_PIN, RST_PIN);  // The rfid instance is created here

void setup()
{
  Serial.begin(9600);
  SPI.begin();    // SPI communication being initialized
  rfid.PCD_Init();
  Serial.println("Put you card on top of the reader");
}

void loop()
{
  int count = 0;    // Read count limiter
  if (! rfid.PICC_IsNewCardPresent())
  {
    return;
  }
  if (! rfid.PICC_ReadCardSerial())
  {
    return;
  }
  String content=""; 
  for (byte i = 0; i < rfid.uid.size; i++)
  {
    content.concat(String(rfid.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(rfid.uid.uidByte[i], HEX));
    count=count+1;
    content.toUpperCase();
    if (prev_content.equals(content))   // If the card is repeated ly placed
    {
      Serial.println("Please move your card from reader");
      delay(2000);
    }
    else if (content.substring(1)=="6B 5D 9C 0B") // If access is grated
    {
      Serial.println("Message : Autorised access");
      Serial.println(content);
      delay(2000);
      prev_content=content;
    }
    else if (count==4)    // If the access is denied
    {
      Serial.println("Message : Access denied");
      Serial.println(content);
      delay(2000);
      prev_content=content;
    }
  }
}
